var formSelector = 'form[name=reason]';

var formCallback = function(data, statusCode) {
    var alertContainer = $('.alert-container');

    alertContainer.find('*').slideUp(400, function() {
        $(this).remove();
    });

    var alertType = 'success';
    if(statusCode == 'error') {
        data = data.responseJSON;
        alertType = 'danger';
    }

    for(i in data) {
        var message = data[i];
        alertContainer.append('<div class="alert alert-' + alertType + (statusCode == 'error' ? ' alert-dismissible' : '') + '" role="alert" style="display:none;">' + (statusCode == 'error' ? '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' : '') + message + '</div>');
        alertContainer.find('*').slideDown();
    }

    if(statusCode == 'success') {
        $(formSelector).slideUp();
    }
}

$(document).ready(function() {
    var form = $(formSelector);

    form.submit(function(event) {
        var form = $(this);
        var url = form.attributes;
        var data = {
            'reason[reason]' : $(this).find('input[name="reason[reason]"]:checked').val(),
            'reason[note]' : $(this).find('textarea[name="reason[note]"]').val(),
            'reason[submit]' : null,
            'reason[_token]' : $(this).find('input[name="reason[_token]"]').val()
        };

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: {
                'reason[reason]' : $(this).find('input[name="reason[reason]"]:checked').val(),
                'reason[note]' : $(this).find('textarea[name="reason[note]"]').val(),
                'reason[submit]' : null,
                'reason[_token]' : $(this).find('input[name="reason[_token]"]').val()
            },
            success: formCallback,
            error: formCallback,
            dataType: 'json'
        });

        event.preventDefault();
    });
});