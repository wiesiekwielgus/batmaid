<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ReasonType extends AbstractType
{
    /** @var integer */
    const CONFLICT_REASON= 0;

    /** @var integer */
    const TRAVEL_REASON = 1;

    /** @var integer */
    const OTHER_REASON = 2;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($options['action'])
            ->add('reason',ChoiceType::class,[
                'label' => 'Reason for declining',
                'expanded' => true,
                'choices' => [
                    'i have a conflicting appointment' => self::CONFLICT_REASON,
                    'the travel time is too long' => self::TRAVEL_REASON,
                    'other' => self::OTHER_REASON
                ],
                'constraints' => [
                    new Callback(function($object, ExecutionContextInterface $context) {
                        $note = $context->getRoot()->getData()['note'];
                        if($object == self::OTHER_REASON && strlen($note) == 0) {
                            $context->addViolation('Please enter reason explanation into textarea');
                        }
//                        die(dump($object));
//                        die(dump($note));
                    })
                ]
            ])
            ->add('note',TextareaType::class,[
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Enter other reason'
                ]
//                'translation_domain'    =>  'AppBundle'
            ])
            ->add('submit',SubmitType::class);

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'validation_groups' => function (FormInterface $form) {
                $groups = array('Default');
                $data = $form->getData();
//die(dump($data));
                if ($data['SOME_OTHER_FIELD']) { // then we want password to be required
                    $groups[] = 'SOME_OTHER_VALIDATION_GROUP';
                }

                return $groups;
            }
        ));
    }

//    /**
//     * @return string
//     */
//    public function getName()
//    {
//        return '';
//    }
}