<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function getForm() {
        return $this->createForm('AppBundle\Form\ReasonType', null, ['action' => $this->generateUrl('reason')]);
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $form = $this->getForm();

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'messages' => []
        ]);
    }

    /**
     * @Route("/reason", name="reason", methods="POST")
     */
    public function reasonAction(Request $request)
    {
        $jsonRequest = in_array('application/json',$request->getAcceptableContentTypes());
        $form = $this->getForm();
        $form->handleRequest($request);

        $messages = [];
        $jsonResponseCode = 200;

        if($form->isValid()) {
            $messages['success'] = ['Thank you for submitting reason of cancelling the job'];;
        } else {
            $jsonResponseCode = 400;

            foreach($form->getErrors(true, true) as $error) {
                $messages['error'][] = $error->getMessage();
            }
        }

        if($jsonRequest) {
            return new JsonResponse(array_merge($messages['success'] ?? [], $messages['error'] ?? []), $jsonResponseCode);
        }

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'messages' => $messages
        ]);
    }
}
